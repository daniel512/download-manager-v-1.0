1. Open bin folder and run Runner.bat
2. Open the DM from task bar
3. Set your download path
4. Run DM again.
5. DM will create some folders in destination which you've saved.
6. Set folder "__incoming" as your default browser download path.

Tested with Chrome v. 73, on Windows 8 and Windows 10.