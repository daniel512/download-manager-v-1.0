//V 1.0

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.nio.file.attribute.BasicFileAttributes;
import java.nio.file.attribute.FileTime;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;

import org.xml.sax.SAXException;

public class DownloadManager {
	
	static String downloadPath;
	static File incoming, textFiles, compressedFiles, executable, images, videos, audios, otherFiles;
	
	static Date currentDate = new Date();

	static String getExtension(File file) {
		String name = file.getName();
		int lastIndexOf = name.lastIndexOf('.');
		if(lastIndexOf == -1) return "";
		
		return name.substring(lastIndexOf).toLowerCase();
	}
	

	
	static File path = new File(DownloadManager.class.getProtectionDomain().getCodeSource().getLocation().getPath());
	static File config = new File(path+"//config.txt");
	
	static String readPath() throws FileNotFoundException{
		if(config.exists()) {
			Scanner scan = new Scanner(config);
			return scan.nextLine();
		} 
		else return "path";
	}
	
	
	
	
	public static void clean() throws IOException {

		ArrayList<File> list = new ArrayList<File>(Arrays.asList(incoming.listFiles()));
		
		if(incoming.listFiles().length > 0) {
			for( int i = incoming.listFiles().length-1; list.get(0).exists(); i-- ) {

				if(!getExtension( list.get(i)).equals(".crdownload") && ((File) list.get(i)).length() > 0 &&
				   !getExtension( list.get(i)).equals(".tmp") )
				
				{
					
					if
					 (
						getExtension(list.get(i)).equals(".txt") ||  
						getExtension(list.get(i)).equals(".doc") ||  
						getExtension(list.get(i)).equals(".docx")||  
						getExtension(list.get(i)).equals(".pdf") ||  
						getExtension(list.get(i)).equals(".rtf") || 
						getExtension(list.get(i)).equals(".tex") || 
						getExtension(list.get(i)).equals(".wpd") ||
						getExtension(list.get(i)).equals(".wps")			
					  )
						{	
							ArrayList<File> files = new ArrayList<File>(Arrays.asList(textFiles.listFiles()));
					
							DownloadChecker.moveFile(files, list.get(i), textFiles);	
						 }
					else if
					(
						getExtension(list.get(i)).equals(".7z")  ||  
						getExtension(list.get(i)).equals(".zip") || 
						getExtension(list.get(i)).equals(".rar") || 
						getExtension(list.get(i)).equals(".z")	 ||
						getExtension(list.get(i)).equals(".arj") ||  
						getExtension(list.get(i)).equals(".pkg") || 
						getExtension(list.get(i)).equals(".rpm") || 
						getExtension(list.get(i)).equals(".deb")
					)
					{
						ArrayList<File> files = new ArrayList<File>(Arrays.asList(compressedFiles.listFiles()));
						
						DownloadChecker.moveFile(files, list.get(i), compressedFiles);
					}	
					else if
					(
						getExtension(list.get(i)).equals(".jpg")  ||  
						getExtension(list.get(i)).equals(".jpeg") || 
						getExtension(list.get(i)).equals(".ai")   || 
						getExtension(list.get(i)).equals(".gif")  ||
						getExtension(list.get(i)).equals(".ico")  ||  
						getExtension(list.get(i)).equals(".png")  || 
						getExtension(list.get(i)).equals(".ps")   || 
						getExtension(list.get(i)).equals(".psd")  ||
						getExtension(list.get(i)).equals(".svg")  || 
						getExtension(list.get(i)).equals(".tif")  ||
						getExtension(list.get(i)).equals(".ps") 
					)
					{
						ArrayList<File> files = new ArrayList<File>(Arrays.asList(images.listFiles()));
						
						DownloadChecker.moveFile(files, list.get(i), images);		
					}	
					else if
					(
						getExtension(list.get(i)).equals(".aif")  ||  
						getExtension(list.get(i)).equals(".cda")  || 
						getExtension(list.get(i)).equals(".mid")  || 
						getExtension(list.get(i)).equals(".midi") ||
						getExtension(list.get(i)).equals(".mp3")  ||  
						getExtension(list.get(i)).equals(".mpa")  || 
						getExtension(list.get(i)).equals(".ogg")  || 
						getExtension(list.get(i)).equals(".wav")  ||
						getExtension(list.get(i)).equals(".wma")  || 
						getExtension(list.get(i)).equals(".wpl")  
					)
					{
						ArrayList<File> files = new ArrayList<File>(Arrays.asList(audios.listFiles()));
						
						DownloadChecker.moveFile(files, list.get(i), audios);			
					}	
					else if
					(
						getExtension(list.get(i)).equals(".3g2")  ||  
						getExtension(list.get(i)).equals(".3gp")  || 
						getExtension(list.get(i)).equals(".avi")  || 
						getExtension(list.get(i)).equals(".flv")  ||
						getExtension(list.get(i)).equals(".h264") ||  
						getExtension(list.get(i)).equals(".m4v")  || 
						getExtension(list.get(i)).equals(".mkv")  || 
						getExtension(list.get(i)).equals(".mov")  ||
						getExtension(list.get(i)).equals(".mp4")  || 
						getExtension(list.get(i)).equals(".mpg")  || 
						getExtension(list.get(i)).equals(".mpeg") ||
						getExtension(list.get(i)).equals(".rm")   || 
						getExtension(list.get(i)).equals(".swf")  || 
						getExtension(list.get(i)).equals(".vob")  ||
						getExtension(list.get(i)).equals(".wmv")  
					)
					{
						ArrayList<File> files = new ArrayList<File>(Arrays.asList(videos.listFiles()));
						
						DownloadChecker.moveFile(files, list.get(i), videos);		
					}
					else if
					(
						getExtension(list.get(i)).equals(".apk")    ||
						getExtension(list.get(i)).equals(".bat")    || 
						getExtension(list.get(i)).equals(".bin")    || 
						getExtension(list.get(i)).equals(".com")    ||
						getExtension(list.get(i)).equals(".exe")    || 
						getExtension(list.get(i)).equals(".gadget") || 
						getExtension(list.get(i)).equals(".jar")    ||
						getExtension(list.get(i)).equals(".wsf")    
					)
					{
						ArrayList<File> files = new ArrayList<File>(Arrays.asList(executable.listFiles()));
						
						DownloadChecker.moveFile(files, list.get(i), executable);
					}
					
					else {
						ArrayList<File> files = new ArrayList<File>(Arrays.asList(otherFiles.listFiles()));
						
						DownloadChecker.moveFile(files, list.get(i), otherFiles);
						}
				
				} else break;

			} 
		}
	}
	
	

	public static void main(String[] args) throws ParseException, IOException, SAXException, MalformedURLException {	

		downloadPath = readPath();
		//
		incoming = new File(downloadPath+"\\__incoming");
		textFiles = new File(downloadPath+"\\Text Files");
		compressedFiles = new File(downloadPath+"\\Compressed Files");
		executable = new File("downloadPath+\"\\Executable Files");
		images = new File(downloadPath+"\\Images");
		videos = new File(downloadPath+"\\Video Files");
		audios = new File(downloadPath+"\\Audio Files");
		otherFiles = new File(downloadPath+"\\Other");
		//
		TrayHandler.tray();
		
		System.out.println(downloadPath+"\\__incoming");
		System.out.println("img Path:" + TrayHandler.imgPath);
		
		File downloads = new File(downloadPath);
		System.out.println("download: " + downloadPath);
		
	
		boolean creatingIncoming = new File(downloadPath+"\\__incoming").mkdir();
		boolean creatingTextFiles = new File(downloadPath+"\\Text Files").mkdir();
		boolean creatingOtherFiles = new File(downloadPath+"\\Other").mkdir();
		boolean creatingAudio = new File(downloadPath+"\\Audio Files").mkdir();
	    boolean creatingVideo = new File(downloadPath+"\\Video Files").mkdir();
	    boolean creatingImages = new File(downloadPath+"\\Images").mkdir();
		boolean creatingCompressed = new File(downloadPath+"\\Compressed Files").mkdir();
		boolean creatingExecutable = new File(downloadPath+"\\Executable Files").mkdir();
	
		  
		
		DownloadChecker.executor.scheduleAtFixedRate(DownloadChecker.task, 0 , 1 , TimeUnit.MILLISECONDS);
		FoldersChecker.foldersCheckerExecutor.scheduleAtFixedRate(FoldersChecker.checkFolders, 0, 10, TimeUnit.MILLISECONDS);

	}
	
	
}