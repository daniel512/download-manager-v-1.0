import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.TextField;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;

class MyButton extends JButton {
	
	MyButton(int x, int y, String text) {
		this(x, y, 45, 45, text);
	}
	
	MyButton(int x, int y, int w, int h, String text) {
		this.setLocation(x,y);
		this.setSize(w,h);
		this.setText(text);
	}
}

public class GUI implements ActionListener{
	
	public boolean RIGHT_TO_LEFT = false;
	public boolean visible = false;
	public JButton btn_zapisz;
	public JTextField pathField;
	
	public GUI() {
		this.createGUI();
	}
	
	public void addComponentsToPane(Container pane) {
		
		if(!(pane.getLayout() instanceof BorderLayout)) {
			pane.add(new JLabel("cos tam cos tam"));
			return;
		}
		
		if(RIGHT_TO_LEFT) {
			pane.setComponentOrientation(java.awt.ComponentOrientation.RIGHT_TO_LEFT);
		}
		
		btn_zapisz = new JButton("Zapisz");
		pane.add(btn_zapisz, BorderLayout.PAGE_END);
		btn_zapisz.addActionListener(this);
		
		JLabel desc = new JLabel("Podaj sciezke folderu docelowego");
		
		pathField = new JTextField("");
		pathField.setPreferredSize(new Dimension(200, 20));

		
		JPanel panel = new JPanel(new GridLayout(5, 1));
		
		panel.add(desc);
		panel.add(pathField);
		panel.add(btn_zapisz);
		
	
		
		pane.add(panel, BorderLayout.LINE_START);
		
	}
	
	public void createGUI() {
		JFrame window = new JFrame("GUI");
		window.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		window.setLocation(200, 300);
		//
		addComponentsToPane(window.getContentPane());
		window.pack();
		window.setVisible(true);
	}
	
	public void setVisible(boolean t) {
		this.visible = t;
	}
	
	public boolean getVisible() {
		return this.visible;
	}
	

	
	public void actionPerformed(ActionEvent e) {
		Object src = e.getSource();
		
		if(src == btn_zapisz) {
			pathField.getText();
			File path = new File(this.getClass().getProtectionDomain().getCodeSource().getLocation().getPath());
			
			System.out.println(path);
			
			try {
				boolean config = new File(path+"//config.txt").createNewFile();
				File file = new File(path+"//config.txt");
				PrintWriter pw = new PrintWriter(file);
				pw.print("");
				pw.println(pathField.getText());
				pw.close();
				JOptionPane.showMessageDialog(null, "Sciezka zostala zapisana. Uruchom program ponownie", "Zapisano", JOptionPane.INFORMATION_MESSAGE);
				System.exit(0);
			} catch (IOException e1) {
				e1.printStackTrace();
			}
		}
	}
}
