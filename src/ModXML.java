import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.w3c.dom.Node;
import org.w3c.dom.Element;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.StringReader;

@SuppressWarnings("unused")
public class ModXML {

 	public static void read(){

    try {
  
	File file = new File("C:\\Users\\Laptop\\Desktop\\TEST23\\plik.xml");
	DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	

	Document doc = dBuilder.parse(file);
			
	//optional, but recommended
	//read this - http://stackoverflow.com/questions/13786607/normalization-in-dom-parsing-with-java-how-does-it-work
	doc.getDocumentElement().normalize();

	System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
			
	NodeList nList = doc.getElementsByTagName("folders");
			
	System.out.println("----------------------------");

	for(int i = 0; i < nList.getLength(); i++) {
		
	Node node = nList.item(i);
	
	if(node.getNodeType() == Node.ELEMENT_NODE) {
		
		Element el = (Element) node;
		System.out.println(el.getAttribute("type"));
		
		for(int j = 1; j < Integer.parseInt(el.getAttribute("numOfFolders")); j++) {	
		System.out.println(el.getElementsByTagName("folder"+(j)).item(0).getTextContent());
		}
	}

	}
	System.out.println("");
	System.out.println("");
	System.out.println("");
	System.out.println("");
			
    } catch (Exception e) {
	e.printStackTrace();
    }
  }

}