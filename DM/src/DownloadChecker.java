import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class DownloadChecker {
	static ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
	
	static Runnable task = new Runnable() {
	public void run() {	
		if(DownloadManager.incoming.listFiles().length > 0) {
			System.out.println("Lista: "+ DownloadManager.incoming.listFiles().length);	
				try {
					System.out.println("try");
					DownloadManager.clean();
					
				} catch (IOException e) {
					e.printStackTrace();
				}	
			
		}
	} 
	};
	
	public static boolean checkSame(ArrayList<File> f1, File f2) {
		boolean result = false;
		for(File file : f1) {
			if(  file.getName().equals(f2.getName()) || file.getName().equals("(2)"+f2.getName())  ) {
				result = true;	
			}	
			else {
				result = false;
			}
		}
		
		return result;
	}
	
	
	public static int sameCount(ArrayList<File> f1, File f2) {
		int result = 0;
		
		for(File file : f1) {
			for(int i = 1; i < 99; i++) {
				if(file.getName().equals("("+i+")"+f2.getName())) {
					++result;
				}
			}
		}
		return result;
	}
	
	
	
	public static void moveFile(ArrayList<File> files, File f2, File folder) throws IOException {
		if(!files.isEmpty()) {
			
			for(File file : files) {
				if(file.getName().equals(f2.getName())) {
					System.out.println("sames" + DownloadChecker.sameCount(files, f2));
					System.out.println("same file");
					Files.move( Paths.get(f2.getAbsolutePath()),
							Paths.get(folder.getAbsolutePath()+"\\"+"("+(DownloadChecker.sameCount(files, f2)+1)+")"+( f2).getName()),
							StandardCopyOption.REPLACE_EXISTING);
					
					break;
				} }
			
				if(f2.exists()) {
					Files.move( Paths.get(( f2).getAbsolutePath()),
							Paths.get(folder.getAbsolutePath()+"\\"+( f2).getName()),
							StandardCopyOption.REPLACE_EXISTING);				
				} 
			
			 } 
		
			else {
				Files.move( Paths.get(( f2).getAbsolutePath()),
						Paths.get(folder.getAbsolutePath()+"\\"+( f2).getName()),
						StandardCopyOption.REPLACE_EXISTING);	
			}		
	}

		
}
