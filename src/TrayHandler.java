import java.awt.AWTException;
import java.awt.Image;
import java.awt.MenuItem;
import java.awt.PopupMenu;
import java.awt.SystemTray;
import java.awt.Toolkit;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

final public class TrayHandler{
	static TrayIcon trayIcon = null;
	static MenuItem item_exit, item_open;
	static String imgPath = DownloadManager.class.getProtectionDomain().getCodeSource().getLocation().getPath();
	
	
	public static void tray() {
		if(SystemTray.isSupported()) {
			SystemTray tray = SystemTray.getSystemTray();
			System.out.println("imgPath:" + imgPath);
			
			Image trayImg = Toolkit.getDefaultToolkit().getImage(
					imgPath+"\\icon.jpg"
					);
			
			ActionListener listener = new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Object src = e.getSource();
				
				if(src == item_exit) {
					System.exit(0);
				}
				
				if(src == item_open) {
					 new GUI();
				}
			}
			};
			
			PopupMenu popup = new PopupMenu();
			
			item_exit = new MenuItem("Exit");
			item_open = new MenuItem("Open");		
			
			item_exit.addActionListener(listener);
			item_open.addActionListener(listener);
			
			popup.add(item_exit);
			popup.add(item_open);
			
			trayIcon = new TrayIcon(trayImg, "DownloadManager v 1.0", popup);
			
			trayIcon.addActionListener(listener);
			
			try {
				tray.add(trayIcon);
			} catch(AWTException e5) {
				e5.printStackTrace();
			}
			
			}
		}
	
}